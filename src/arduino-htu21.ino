/*
 * arduino-htu21.ino - Firmware for testign HTU21 Temperature and humidity sensor on Arduino
 *
 * Copyright 2015 Tomas Hozza <thozza@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 * Tomas Hozza <thozza@gmail.com>
 *
 * Adafruit HTU21 library - https://github.com/adafruit/Adafruit_HTU21DF_Library
 *
 * HTU21 - http://www.meas-spec.com/downloads/HTU21D.pdf
 */

#include <Wire.h>
#include <Adafruit_HTU21DF.h>

#define SERIAL_SPEED 9600


Adafruit_HTU21DF sensor = Adafruit_HTU21DF();

void setup() {
    Serial.begin(SERIAL_SPEED);
    Serial.println("Testing sketch for HTU21 sensor");

    if (!sensor.begin()) {
        Serial.println("Sensor not found!");
        while (true);
    }
}

void loop() {
    Serial.print("T=");
    Serial.print(sensor.readTemperature());
    Serial.print("\t");
    Serial.print("H=");
    Serial.print(sensor.readHumidity());
    Serial.println();

    delay(1000);
}
