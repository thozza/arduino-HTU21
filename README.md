# Arduino sketch for testing HTU21 temperature and humidity sensor
This repository includes the firmware for testing HTU21 Temperature and Humidity sensor. The
firmware is designed for Arduino Uno board. For
building the firmware, the use of [PlatformIO](http://platformio.org)
tool is assumed.

## Getting the source
Used libraries are added in this repository as git submodules. Before building
the firmware it is important to update the git submodules by running:
```
git submodule update --init --recursive
```

## Building the firmware
Building is easy as running a single command:
```
platformio run
```

## Uploading the firmware
Uploading is again super easy by running:
```
platformio run --target upload
```

## Connecting to the serial port
Just run:
```
platformio serialports monitor
```
